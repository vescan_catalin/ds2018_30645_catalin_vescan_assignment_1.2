package model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;

@Entity
@Table(name = "plane", schema = "flight")
public class PlaneEntity {
    private int id;
    private int planeId;
    private String airplaneType;
    private Date departureDate;
    private Date arrivalDate;
    private int departureCity;
    private int arrivalCity;
    private Collection<BookingEntity> bookingsById;
    private CityEntity cityByDepartureCity;
    private CityEntity cityByArrivalCity;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "planeId")
    public int getPlaneId() {
        return planeId;
    }

    public void setPlaneId(int planeId) {
        this.planeId = planeId;
    }

    @Basic
    @Column(name = "airplaneType")
    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    @Basic
    @Column(name = "departureDate")
    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    @Basic
    @Column(name = "arrivalDate")
    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlaneEntity that = (PlaneEntity) o;

        if (id != that.id) return false;
        if (planeId != that.planeId) return false;
        if (airplaneType != null ? !airplaneType.equals(that.airplaneType) : that.airplaneType != null) return false;
        if (departureDate != null ? !departureDate.equals(that.departureDate) : that.departureDate != null)
            return false;
        if (arrivalDate != null ? !arrivalDate.equals(that.arrivalDate) : that.arrivalDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + planeId;
        result = 31 * result + (airplaneType != null ? airplaneType.hashCode() : 0);
        result = 31 * result + (departureDate != null ? departureDate.hashCode() : 0);
        result = 31 * result + (arrivalDate != null ? arrivalDate.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "planeByPlaneId")
    public Collection<BookingEntity> getBookingsById() {
        return bookingsById;
    }

    public void setBookingsById(Collection<BookingEntity> bookingsById) {
        this.bookingsById = bookingsById;
    }

    @ManyToOne
    @JoinColumn(name = "departureCity", referencedColumnName = "id", insertable = false, updatable = false)
    public CityEntity getCityByDepartureCity() {
        return cityByDepartureCity;
    }

    public void setCityByDepartureCity(CityEntity cityByDepartureCity) {
        this.cityByDepartureCity = cityByDepartureCity;
    }

    @ManyToOne
    @JoinColumn(name = "arrivalCity", referencedColumnName = "id", insertable = false, updatable = false)
    public CityEntity getCityByArrivalCity() {
        return cityByArrivalCity;
    }

    public void setCityByArrivalCity(CityEntity cityByArrivalCity) {
        this.cityByArrivalCity = cityByArrivalCity;
    }





    @JoinColumn(name = "departureCity", referencedColumnName = "id")
    public int getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(int departureCity) {
        this.departureCity = departureCity;
    }

    @JoinColumn(name = "arrivalCity", referencedColumnName = "id")
    public int getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(int arrivalCity) {
        this.arrivalCity = arrivalCity;
    }
}
