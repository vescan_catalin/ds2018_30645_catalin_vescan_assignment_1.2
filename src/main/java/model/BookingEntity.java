package model;

import javax.persistence.*;

@Entity
@Table(name = "booking", schema = "flight")
public class BookingEntity {
    private int id;
    private AdminEntity adminByClientId;
    private UserEntity userByClientId;
    private PlaneEntity planeByPlaneId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookingEntity that = (BookingEntity) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "clientId", referencedColumnName = "id")
    public AdminEntity getAdminByClientId() {
        return adminByClientId;
    }

    public void setAdminByClientId(AdminEntity adminByClientId) {
        this.adminByClientId = adminByClientId;
    }

    @ManyToOne
    @JoinColumn(name = "clientId", referencedColumnName = "id", insertable = false, updatable = false)
    public UserEntity getUserByClientId() {
        return userByClientId;
    }

    public void setUserByClientId(UserEntity userByClientId) {
        this.userByClientId = userByClientId;
    }

    @ManyToOne
    @JoinColumn(name = "planeId", referencedColumnName = "id")
    public PlaneEntity getPlaneByPlaneId() {
        return planeByPlaneId;
    }

    public void setPlaneByPlaneId(PlaneEntity planeByPlaneId) {
        this.planeByPlaneId = planeByPlaneId;
    }
}
