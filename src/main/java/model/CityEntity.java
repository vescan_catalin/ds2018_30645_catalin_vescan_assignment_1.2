package model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "city", schema = "flight")
public class CityEntity {
    private int id;
    private String name;
    private double latitude;
    private double longitute;
    private Collection<PlaneEntity> planesById;
    private Collection<PlaneEntity> planesById_0;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "latitude")
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Basic
    @Column(name = "longitute")
    public double getLongitute() {
        return longitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityEntity that = (CityEntity) o;

        if (id != that.id) return false;
        if (latitude != that.latitude) return false;
        if (longitute != that.longitute) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        double result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + latitude;
        result = 31 * result + longitute;
        return (int) result;
    }

    @OneToMany(mappedBy = "cityByDepartureCity")
    public Collection<PlaneEntity> getPlanesById() {
        return planesById;
    }

    public void setPlanesById(Collection<PlaneEntity> planesById) {
        this.planesById = planesById;
    }

    @OneToMany(mappedBy = "cityByArrivalCity")
    public Collection<PlaneEntity> getPlanesById_0() {
        return planesById_0;
    }

    public void setPlanesById_0(Collection<PlaneEntity> planesById_0) {
        this.planesById_0 = planesById_0;
    }
}
