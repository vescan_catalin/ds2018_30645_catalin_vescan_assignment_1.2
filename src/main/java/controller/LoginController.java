package controller;

import connection.Connection;
import model.AdminEntity;
import model.UserEntity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoginController {

    public static final int admin = 0, user = 1, plane = 2, city = 3;

    public Connection connection = new Connection();

    public List<Object> list = new ArrayList<Object>();

    public Object checkAccount(String username, String password, String securityKey) {
        list.clear();

        if(!securityKey.equals(""))
            list = connection.getAll(admin);
        else
            list = connection.getAll(user);

        if(list != null) {
            if(list.get(0).getClass().equals(AdminEntity.class)) {
                return list
                        .stream()
                        .filter(x -> ((AdminEntity)x).getSecurityKey().equals(securityKey))
                        .findFirst()
                        .get();
            } else {
                return list
                        .stream()
                        .filter(x -> ((UserEntity)x).getUsername().equals(username)
                                && ((UserEntity)x).getPassword().equals(password))
                        .findFirst()
                        .get();
            }
        }

        return null;
    }

    public List getAll(int flag) {
        list.clear();
        list = connection.getAll(flag);

        if(list != null)
            return list;

        return null;
    }

    public void remove(int flag, int id) {
        connection.remove(flag, id);
    }

    public void update(int flag, int id, String airplaneType) {
        connection.update(flag, id, airplaneType);
    }

    public void save(Object o) {
        connection.save(o);
    }
}
