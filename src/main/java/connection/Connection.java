package connection;

import model.AdminEntity;
import model.CityEntity;
import model.PlaneEntity;
import model.UserEntity;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import javax.swing.*;
import java.util.List;

public class Connection {

    private Session session;

    public Connection() {
        session = new Configuration().configure().buildSessionFactory().openSession();
        session.beginTransaction();
        System.out.println("conexiune realizata");
    }

    public Session getSession() {
        return session;
    }

    public void closeSession() {
        session.close();
    }

    public void save(Object o) {
        if(session != null) {
            session.save(o);
            session.getTransaction().commit();
        } else
            JOptionPane.showMessageDialog(null, "Error",
                    "Can't connect to database!", JOptionPane.ERROR_MESSAGE);
    }

    /* 0 - admins
     * 1 - users
     * 2 - flights
     * 3 - cities
     */
    public List getAll(int  flag) {
        if(session != null)
            switch(flag) {
                case 0: return session.createNativeQuery("select * from admin", AdminEntity.class).list();
                case 1: return session.createNativeQuery("select * from user", UserEntity.class).list();
                case 2: return session.createNativeQuery("select * from plane", PlaneEntity.class).list();
                case 3: return session.createNativeQuery("select * from city", CityEntity.class).list();
                default: {
                    JOptionPane.showMessageDialog(null, "Error",
                        "Can't connect to database!", JOptionPane.ERROR_MESSAGE);
                    return null;
                }
            }
        return null;
    }

    public void remove(int flag, int id) {
        if(session != null)
            switch(flag) {
                case 0: {
                    session.createNativeQuery("delete from admin where id = (:id)")
                            .setParameter("id", id);
                    session.getTransaction().commit();
                } break;
                case 1: {
                    session.createNativeQuery("delete from user where id = (:id)")
                            .setParameter("id", id);
                    session.getTransaction().commit();
                } break;
                case 2: {
                    session.createNativeQuery("delete from plane where id = (:id)")
                            .setParameter("id", id)
                            .executeUpdate();
                    session.getTransaction().commit();
                } break;
                case 3: {
                    session.createNativeQuery("delete from city where id = (:id)")
                            .setParameter("id", id);
                    session.getTransaction().commit();
                } break;
                default: JOptionPane.showMessageDialog(null, "Error",
                        "Can't connect to database!", JOptionPane.ERROR_MESSAGE);
            }


    }

    public void update(int flag, int id, String data) {
        if(session != null)
            switch(flag) {
                case 0: {
                 //   session.createNativeQuery("update admin set airplaneType = :type where id = (:id)")
                    // .setParameter("id", id)
                    // .executeUpdate();
                   // session.getTransaction().commit();
                } break;
                case 1: {
                //    session.createNativeQuery("update user set airplaneType = :type where id = (:id)")
                    // .setParameter("id", id)
                    // .executeUpdate();
                //    session.getTransaction().commit();
                } break;
                case 2: {
                    session.createNativeQuery("update plane set airplaneType = '" + data + "' where id = (:id)")
                            .setParameter("id", id)
                            .executeUpdate();
                    session.getTransaction().commit();
                } break;
                case 3: {
                  //  session.createNativeQuery("update city set airplaneType = :type where id = (:id)")
                    // .setParameter("id", id)
                    // .executeUpdate();
                  //  session.getTransaction().commit();
                } break;
                default: JOptionPane.showMessageDialog(null, "Error",
                        "Can't connect to database!", JOptionPane.ERROR_MESSAGE);
            }

    }
}
