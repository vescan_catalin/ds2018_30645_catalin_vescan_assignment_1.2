<%--
  Created by IntelliJ IDEA.
  User: VESCAN
  Date: 10/30/2018
  Time: 10:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
    <h1>Welcome ${username}!</h1>
    <p1>Please choose 1 of the following options:</p1>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to create a new flight --%>
    <form action="admin" method="post">
        planeId: <input type="text" name="planeId" width="10" />
        planeType: <input type="text" name="planeType" width="20" />
        <p>&nbsp;</p>
        departureCity: <input type="text" name="departureCity" width="20" />
        departureDate: <input type="text" name="departureDate" width="20" />
        <p>&nbsp;</p>
        arrivalCity: <input type="text" name="arrivalCity" width="20" />
        arrivalDate: <input type="text" name="arrivalDate" width="20" />
        <p>&nbsp;</p>
        <input type="submit" name="universalName" value="addFlights"/>
    </form>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to read all flights --%>
    <form action="admin" method="get">
        <input type="submit" name="viewFlights" value="viewFlights"/>
    </form>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to update a flight --%>
    <form action="admin" method="post">
        choose the flight id you want to update: <input type="text" name="updateFlightById" width="20" />
        airplane type: <input type="text" name="airplaneType", width="50" />
        <p>&nbsp;</p>
        <input type="submit" name="universalName" value="updateFlight"/>
    </form>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to delete a flight --%>
    <form action="admin" method="post">
        choose the flight id you want to remove: <input type="text" name="removeFlightById" width="20" />
        <p>&nbsp;</p>
        <input type="submit" name="universalName" value="removeFlight"/>
    </form>


</body>
</html>
