<%--
  Created by IntelliJ IDEA.
  User: VESCAN
  Date: 10/31/2018
  Time: 8:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User</title>
</head>
<body>
    <h1>Welcome ${username}!</h1>
    <p1>Please choose 1 of the following options:</p1>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to read all flights --%>
    <form action="user" method="get">
        <input type="submit" name="viewFlights" value="viewFlights"/>
    </form>

    <%-- empty line --%>
    <p>&nbsp;</p>

    <%-- form to call for local time --%>
    <form action="user" method="get">
        <input type="submit" name="getLocalTime" value="getLocalTime"/>
    </form>
</body>
</html>
