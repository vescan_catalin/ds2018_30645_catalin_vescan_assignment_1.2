package servlets;

import controller.LoginController;
import model.AdminEntity;
import model.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

@WebServlet(name = "login")
public class Login extends HttpServlet {

    public LoginController loginController = new LoginController();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String securityKey = request.getParameter("securityKey");

        if(!securityKey.equals("")) {
            AdminEntity admin = (AdminEntity) loginController.checkAccount(username, password, securityKey);
            if(admin != null) {
                request.setAttribute("username", request.getParameter("username"));
                request.getRequestDispatcher("/admin.jsp").forward(request, response);
            }
            else {
                JOptionPane.showMessageDialog(null, "ERROR", "Invalid credentials", JOptionPane.ERROR_MESSAGE);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        } else {
            UserEntity user = (UserEntity) loginController.checkAccount(username, password, securityKey);
            if (user != null) {
                request.setAttribute("username", request.getParameter("username"));
                request.getRequestDispatcher("/user.jsp").forward(request, response);
            }
            else {
                JOptionPane.showMessageDialog(null, "ERROR", "Invalid credentials", JOptionPane.ERROR_MESSAGE);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
