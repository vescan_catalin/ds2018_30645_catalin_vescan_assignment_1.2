package servlets;

import controller.LoginController;
import model.CityEntity;
import model.PlaneEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.List;

@WebServlet(name = "admin")
public class Admin extends HttpServlet {
    public LoginController loginController;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        loginController = new LoginController();

        String universalName = request.getParameter("universalName");

        switch(universalName) {
            case "addFlights": addFlight(request, response); break;
            case "removeFlight": remove(request, response); break;
            case "updateFlight": update(request, response); break;
            default: return;
        }

        loginController.connection.closeSession();
        response.sendRedirect("/admin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = "Flights timetable";
        String doctype = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "  <title>\n" + title + "\n</title>\n" +
                "</head>" +
                "<body>\n\n";
        String header = "<h1 align = \"center\"> " + title + " </h1>\n";
        String tableStart = "<table style=\"width:100%\">\n";
        String tableColumn = "<tr>\n" +
                "  <th>Flight nr.</th>\n" +
                "  <th>Plane id</th>\n" +
                "  <th>Plane type</th>\n" +
                "  <th>Departure city</th>\n" +
                "  <th>Departure date</th>\n" +
                "  <th>Arrival city</th>\n" +
                "  <th>Arrival date</th>\n" +
                "</tr>\n";
        String tableClose = "</table>\n\n" +
                "</body>\n" +
                "</html>";

        PrintWriter p = response.getWriter();
        p.print(doctype + header + tableStart + tableColumn);

        LoginController loginController = new LoginController();
        List<PlaneEntity> flights = loginController.getAll(LoginController.plane);

        flights.forEach(x -> p.print("<tr>\n" +
                "  <td align=\"center\">" + x.getId() + "</td>\n" +
                "  <td align=\"center\">" + x.getPlaneId() + "</td>\n" +
                "  <td align=\"center\">" + x.getAirplaneType() + "</td>\n" +
                "  <td align=\"center\">" + x.getCityByDepartureCity().getName() + "</td>\n" +
                "  <td align=\"center\">" + x.getDepartureDate() + "</td>\n" +
                "  <td align=\"center\">" + x.getCityByArrivalCity().getName() + "</td>\n" +
                "  <td align=\"center\">" + x.getArrivalDate() + "</td>\n" +
                "</tr>\n"));

        p.print(tableClose);
    }

    public void addFlight(HttpServletRequest request, HttpServletResponse response) {
        int planeId = Integer.parseInt(request.getParameter("planeId"));
        String planeType = request.getParameter("planeType");
        int departureCity = Integer.parseInt(request.getParameter("departureCity"));
        Date departureDate = null;
        int arrivalCity = Integer.parseInt(request.getParameter("arrivalCity"));
        Date arrivalDate = null;

        PlaneEntity flight = new PlaneEntity();
        flight.setPlaneId(planeId);
        flight.setAirplaneType(planeType);
        flight.setDepartureCity(departureCity);
        flight.setDepartureDate(departureDate);
        flight.setArrivalCity(arrivalCity);
        flight.setArrivalDate(arrivalDate);

        loginController.save(flight);
    }

    public void remove(HttpServletRequest request, HttpServletResponse response) {
        String id = request.getParameter("removeFlightById");
        int removeFlightById;

        if(!id.equals("")) {
            removeFlightById = Integer.parseInt(id);
            loginController.remove(LoginController.plane, removeFlightById);
        } else
            JOptionPane.showMessageDialog(null, "ERROR",
                    "This flight doesn't exist!", JOptionPane.ERROR_MESSAGE);
    }

    public void update(HttpServletRequest request, HttpServletResponse response) {
        String airplaneType = request.getParameter("airplaneType");
        String airplaneId = request.getParameter("updateFlightById");
        int id;

        if(!airplaneType.equals("") && !airplaneId.equals("")) {
            id = Integer.parseInt(airplaneId);
            loginController.update(LoginController.plane, id, airplaneType);
        }
        else
            JOptionPane.showMessageDialog(null, "ERROR",
                    "Invalid data!", JOptionPane.ERROR_MESSAGE);
    }
}
