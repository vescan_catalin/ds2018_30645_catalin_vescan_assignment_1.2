package servlets;

import controller.LoginController;
import model.CityEntity;
import model.PlaneEntity;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Stream;

@WebServlet(name = "user")
public class User extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = "Flights timetable";
        String doctype = "<!DOCTYPE html>\n" +
                         "<html>\n" +
                         "<head>\n" +
                         "  <title>\n" + title + "\n</title>\n" +
                         "</head>" +
                         "<body>\n\n";
        String header = "<h1 align = \"center\"> " + title + " </h1>\n";
        String tableStart = "<table style=\"width:100%\">\n";
        String tableColumn = "<tr>\n" +
                             "  <th>Flight nr.</th>\n" +
                             "  <th>Plane id</th>\n" +
                             "  <th>Plane type</th>\n" +
                             "  <th>Departure city</th>\n" +
                             "  <th>Departure date</th>\n" +
                             "  <th>Arrival city</th>\n" +
                             "  <th>Arrival date</th>\n" +
                             "</tr>\n";
        String tableClose = "</table>\n\n" +
                            "</body>\n" +
                            "</html>";

        PrintWriter p = response.getWriter();
        p.print(doctype + header + tableStart + tableColumn);

        LoginController loginController = new LoginController();
        List<PlaneEntity> flights = loginController.getAll(LoginController.plane);

        flights.forEach(x -> p.print("<tr>\n" +
                                     "  <td align=\"center\">" + x.getId() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getPlaneId() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getAirplaneType() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getCityByDepartureCity().getName() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getDepartureDate() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getCityByArrivalCity().getName() + "</td>\n" +
                                     "  <td align=\"center\">" + x.getArrivalDate() + "</td>\n" +
                                     "</tr>\n"));

        p.print(tableClose);
    }
}
